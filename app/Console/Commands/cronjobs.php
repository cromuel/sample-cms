<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Cron\CronExpression;
use Panlatent\CronExpressionDescriptor\ExpressionDescriptor;
use Panlatent\CronExpressionDescriptor\ExpressionParser;

class cronjobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sched:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'run sched on table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $cronexp = (new ExpressionParser('0 30 3 * * ?'))->parse();//(new ExpressionDescriptor('0 30 3 * * ?'));
        $cron = CronExpression::factory('30 3 * *');

        // // Remember, most methods return a DateTime object
        // // echo $cron->getPreviousRunDate(null, 5)->format('Y-m-d H:i:s');

        // foreach ($cron->getMultipleRunDates(5, 'now', true) as $date) {
        //     echo $date->format('Y-m-d H:i:s') . PHP_EOL;
        // }

        $this->info($cronexp);

        // dd($cronexp);
    }
}
