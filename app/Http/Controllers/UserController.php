<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Models\User;

use Response;
use Storage;
use Flash;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;
    public $page_title = 'User';

    public function __construct(UserRepository $userRepo)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->all();

        return view('users.index', [
            'page_title' => $this->page_title.' List',
            'users' => $users
        ]);
    }

    public function all()
    {
        $users = User::where('deleted_at','=', null);

        return Datatables::of($users)
        ->addColumn('role', function ($user) {

                return "".$user->roles->name."";
            })
        ->addColumn('action', function ($user) {

                $show = route('users.show', ['id' => $user->id, 't' => '1']);
                $delete = route('users.destroy', [$user->id]);
                $csrf = csrf_token();
                $deletemodal = "<div id='myModal-".$user->id."' class='modal fade'>
                                    <div class='modal-dialog modal-confirm'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>          
                                                <h4 class='modal-title'>Confirmation</h4>   
                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                            </div>
                                            <div class='modal-body'>
                                                <p>Are you sure you want to delete <b>".$user->name."</b>?</p>
                                                <p>This action cannot be undone and you will be unable to recover any data.</p>
                                            </div>
                                            <div class='modal-footer'>
                                                <a href='#' class='btn btn-info' data-dismiss='modal'>Cancel</a>
                                                <form class='form-delete' role='form' method='POST' action='".$delete."' onsubmit='return LoadingOverlay()'>
                                                    <input name='_method' type='hidden' value='DELETE'> 
                                                    <input name='_token' type='hidden' value='".$csrf."'>
                                                    <input id='".$user->id."' name='".$user->id."' type='hidden'> 
                                                    <button type='submit' class='btn btn-danger'>Delete</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>"; 
                return "<center><div class='btn-group'>
                            <a href='".$show."' onclick='return LoadingOverlay()' class='btn btn-default btn-xs'>
                            <i class='fa fa-eye'></i>
                            </a>
                            <a href='#' class='btn btn-danger btn-xs' data-toggle='modal' data-target='#myModal-".$user->id."'>
                                <i class='fa fa-trash'></i>
                            </a>
                        </div></center>

                        ".$deletemodal."";
            })
        ->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create', [
            'page_title' => 'New '.$this->page_title
        ]);
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $image_path = null;
        if ($request->hasFile('photo')) {
            $image_path = Storage::putFile('public/'.$this->page_title, $request->file('photo'));
        }  

        $input = (object) [];
        $input->name = $request->fname.' '.$request->lname;
        $input->fname = $request->fname;
        $input->lname = $request->lname;
        $input->phone = $request->phone;
        $input->email = $request->email;
        $input->role = $request->role;
        $input->photo = $image_path;
        $input->password = bcrypt('12345');

        $user = $this->userRepository->create($input);

        return redirect(route('users.index'))->with('success', 'User saved successfully.');
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id, Request $request)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return redirect(route('users.index'))->with('destroy', 'User not found');
        }

        return view('users.show', [
            'page_title' => $this->page_title.' List',
            'page_title_sub' => $this->page_title.' Profile',
            't' => $request->t,
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return redirect(route('users.index'))->with('destroy', 'User not found');
        }

        return view('users.edit', [
            'page_title' => $this->page_title.' List',
            'user' => $user
        ]);
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return redirect(route('users.index'))->with('destroy', 'User not found');
        }

        $input = (object) [];
        $input->name = $request->fname.' '.$request->lname;
        $input->fname = $request->fname;
        $input->lname = $request->lname;
        $input->phone = $request->phone;
        $input->email = $request->email;
        $input->role = $request->role;

        if ($request->hasFile('photo')) {
            Storage::delete([$user->photo]);
            $input->photo = Storage::putFile('public/'.$this->page_title, $request->file('photo'));
        }

        $user = $this->userRepository->update((array) $input, $id);

        return redirect(route('users.index'))->with('success', 'User updated successfully.');
    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return redirect(route('users.index'))->with('destroy', 'User not found');
        }

        $this->userRepository->delete($id);

        return redirect(route('users.index'))->with('destroy', 'User deleted successfully.');
    }
}
