<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Repositories\RoleRepository;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Models\Role;

use Flash;
use Response;

class RoleController extends AppBaseController
{
    /** @var  RoleRepository */
    private $roleRepository;
    public $page_title = 'User Role';

    public function __construct(RoleRepository $roleRepo)
    {
        $this->middleware('auth');
        $this->roleRepository = $roleRepo;
    }

    /**
     * Display a listing of the Role.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return view('roles.index', [
            'page_title' => $this->page_title.' List'
        ]);
    }

    public function all()
    {
        $roles = Role::where('deleted_at','=', null);

        return Datatables::of($roles)
        ->addColumn('action', function ($role) {

                $edit = route('roles.edit', [$role->id]);
                $delete = route('roles.destroy', [$role->id]);
                $csrf = csrf_token();
                $deletemodal = "<div id='myModal-".$role->id."' class='modal fade'>
                                    <div class='modal-dialog modal-confirm'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>          
                                                <h4 class='modal-title'>Confirmation</h4>   
                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                            </div>
                                            <div class='modal-body'>
                                                <p>Are you sure you want to delete <b>".$role->name."</b>?</p>
                                                <p>This action cannot be undone and you will be unable to recover any data.</p>
                                            </div>
                                            <div class='modal-footer'>
                                                <a href='#' class='btn btn-info' data-dismiss='modal'>Cancel</a>
                                                <form class='form-delete' role='form' method='POST' action='".$delete."' onsubmit='return LoadingOverlay()'>
                                                    <input name='_method' type='hidden' value='DELETE'> 
                                                    <input name='_token' type='hidden' value='".$csrf."'>
                                                    <input id='".$role->id."' name='".$role->id."' type='hidden'> 
                                                    <button type='submit' class='btn btn-danger'>Delete</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>";
                                 
                return "<center><div class='btn-group'>
                            <a href='".$edit."' onclick='return LoadingOverlay()' class='btn btn-default btn-xs'>
                                <i class='fa fa-edit'></i>
                            </a>
                            <a href='#' class='btn btn-danger btn-xs' data-toggle='modal' data-target='#myModal-".$role->id."'>
                                <i class='fa fa-trash'></i>
                            </a>
                        </div></center>

                        ".$deletemodal."";
            })
        ->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new Role.
     *
     * @return Response
     */
    public function create()
    {
        return view('roles.create', [
            'page_title' => 'New '.$this->page_title
        ]);
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param CreateRoleRequest $request
     *
     * @return Response
     */
    public function store(CreateRoleRequest $request)
    {
        $input = $request->all();

        $role = $this->roleRepository->create($input);

        return redirect(route('roles.index'))->with('success', 'Role saved successfully.');
    }

    /**
     * Display the specified Role.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $role = $this->roleRepository->find($id);

        if (empty($role)) {
            return redirect(route('roles.index'))->with('destroy', 'Role not found');
        }

        return view('roles.show', [
            'page_title' => $this->page_title.' List',
            'role' => $role
        ]);
    }

    /**
     * Show the form for editing the specified Role.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $role = $this->roleRepository->find($id);

        if (empty($role)) {
            return redirect(route('roles.index'))->with('destroy', 'Role not found');
        }

        return view('roles.edit', [
            'page_title' => $this->page_title.' List',
            'page_title_sub' => 'Update '.$this->page_title,
            'role' => $role
        ]);
    }

    /**
     * Update the specified Role in storage.
     *
     * @param int $id
     * @param UpdateRoleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoleRequest $request)
    {
        $role = $this->roleRepository->find($id);

        if (empty($role)) {
            return redirect(route('roles.index'))->with('destroy', 'Role not found');
        }

        $role = $this->roleRepository->update($request->all(), $id);

        return redirect(route('roles.index'))->with('success', 'Role updated successfully.');
    }

    /**
     * Remove the specified Role from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $role = $this->roleRepository->find($id);

        if (empty($role)) {
            return redirect(route('roles.index'))->with('destroy', 'Role not found');
        }

        $this->roleRepository->delete($id);

        return redirect(route('roles.index'))->with('destroy', 'Role deleted successfully.');
    }
}
