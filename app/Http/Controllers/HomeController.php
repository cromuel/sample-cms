<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /** @var  UserRepository */
    private $userRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard/v3', [
            'page_title' => "Dashboard"
        ]);
    }

    public function show(Request $request)
    {
        $user = $this->userRepository->find(Auth::user()->id);

        if (empty($user)) {
            return redirect(route('Home'))->with('destroy', 'User not found');
        }

        return view('users.show', [
            'page_title' => 'Dashboard',
            'page_title_sub' => 'User Profile',
            't' => $request->t,
            'user' => $user
        ]);
    }
}
