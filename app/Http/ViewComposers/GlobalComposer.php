<?php

namespace App\Http\ViewComposers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use App\Models\Role;
use App\Usermenu;
use App\User;

class GlobalComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    public function __construct(User $users)
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Auth::check()){

            // get all user info;
            $global_user = User::find(Auth::user()->id);

            // get all user menu;
            if ($global_user->role == 1) {
                $global_menu = Usermenu::where('status', '=', 0)->get();
            } else {
                $global_menu = Usermenu::where('role', '=', $global_user->role)->where('status', '=', 0)->get();
            }

            // get all roles;
            $global_roles = Role::limit(50)->get();
            
            
            // return result;
            $view->with('global_menu', $global_menu);
            $view->with('global_user', $global_user);
            $view->with('global_roles', $global_roles);
        }

    }
}
