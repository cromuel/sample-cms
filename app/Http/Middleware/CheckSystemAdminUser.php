<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;

use App\Models\User;

class CheckSystemAdminUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            
            $user = User::find(Auth::user()->id);

            if($user->role != 1){
               return redirect('/home');
            }

            return $next($request);

        }else{

            return $next($request);

        }
    }
}
