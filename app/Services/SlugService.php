<?php

namespace App\Services;
use Illuminate\Support\Facades\Auth;


class SlugService
{

    /**
     * set path
     *
     * @return void
     */
    static function slug($string)
    {
        
        // explode by /
        $explode_path = explode(' ', str_replace('/','',$string));

        // return back to his format
        $implode_path = implode('-',$explode_path);

        return $implode_path;
        
    }

}
