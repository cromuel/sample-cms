<?php

namespace App\Services;


class NumberSuffixService
{

    /**
     * int value
     *
     * ! Deprecated
     *
     * @return void
     */
    static function suffix($value)
    {

        if (!in_array(($value % 100),array(11,12,13))){
          switch ($value % 10) {
            // Handle 1st, 2nd, 3rd
            case 1:  return $value .'st';
            case 2:  return $value .'nd';
            case 3:  return $value .'rd';
          }
        }
        return $value .'th';

    }

}
