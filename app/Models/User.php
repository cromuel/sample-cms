<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version June 10, 2019, 5:08 am UTC
 *
 * @property string fname
 * @property string lname
 * @property string name
 * @property string phone
 * @property string email
 * @property string field
 */
class User extends Model
{
    use SoftDeletes;

    public $table = 'users';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'fname',
        'lname',
        'name',
        'phone',
        'email',
        'password',
        'role',
        'photo',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fname' => 'string',
        'lname' => 'string',
        'name' => 'string',
        'phone' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fname' => 'required',
        'lname' => 'required',
        'phone' => 'required',
        'email' => 'unique:users|required',
        'role' => 'not_in:0',
        'photo' => 'image|mimes:jpg,png,jpeg|max:2048'
    ];

    public static $rulesU = [
        'fname' => 'required',
        'lname' => 'required',
        'phone' => 'required',
        'role' => 'not_in:0',
        'photo' => 'image|mimes:jpg,png,jpeg|max:2048'
    ];

    public function roles()
    {
        return $this->belongsTo('App\Models\Role','role');
    }

    
}
