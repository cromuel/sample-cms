<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usermenu extends Model
{
    public function submenu()
    {
        return $this->hasMany('App\Usersubmenu','mainmenu');
    }
}
