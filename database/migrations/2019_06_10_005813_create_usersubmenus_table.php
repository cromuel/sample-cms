<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersubmenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersubmenus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mainmenu')->nullable();
            $table->integer('order')->nullable();
            $table->string('name')->nullable();
            $table->string('activename')->nullable();
            $table->string('icon')->nullable();
            $table->string('route')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersubmenus');
    }
}
