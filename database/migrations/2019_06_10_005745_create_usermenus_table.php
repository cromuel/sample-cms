<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsermenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usermenus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->nullable();
            $table->string('name')->nullable();
            $table->string('activename')->nullable();
            $table->string('icon')->nullable();
            $table->string('route')->nullable();
            $table->boolean('type')->default(0);
            $table->integer('role')->default(2);
            $table->boolean('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usermenus');
    }
}
