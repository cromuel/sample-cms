<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // Default Users
        DB::table('users')->insert([
            'fname' => 'crom',
            'lname' => 'uel',
            'name' => 'cromuel',
            'email' => 'cromuel@me.com',
            'password' => bcrypt('cromuel@me.com'),
            'role' => 1
        ]);

        DB::table('users')->insert([
            'fname' => 'crom',
            'lname' => 'uel',
            'name' => 'cromuel2',
            'email' => 'cromuel2@me.com',
            'password' => bcrypt('cromuel2@me.com'),
            'role' => 2
        ]);
        
    }
}
