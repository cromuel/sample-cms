<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    /**
     * if error run 
     *      composer dump-autoload
     *      php artisan db:seed
     */

    public function run()
    {
        // default Users
        $this->call(UsersSeeder::class);
        // default Roles
        $this->call(RolesSeeder::class);
        // default Menu
        $this->call(MenuSeeder::class);

        // data Faker
        // $faker = Faker\Factory::create();
        // for($i = 0; $i < 20000; $i++) {
        //     App\Models\User::create([
        //         'fname' => $faker->name,
        //         'lname' => $faker->name,
        //         'name' => $faker->unique()->email,
        //         'email' => $faker->email,
        //         'password' => bcrypt($faker->email),
        //         'role' => 1
        //     ]);
        // }
    }
}
