<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // Default Menu Dashboard
        DB::table('usermenus')->insert([
            'order' => 1,
            'name' => 'Dashboard',
            'activename' => 'Dashboard',
            'icon' => 'fa-dashboard',
            'route' => 'Home',
            'type' => 0,
            'role' => 2,
            'status' => 0
        ]);

        // Default Menu with subitems Doctor
        DB::table('usermenus')->insert([
            'order' => 2,
            'name' => 'Doctor',
            'activename' => '',
            'icon' => 'fa-stethoscope',
            'route' => '#',
            'type' => 1,
            'role' => 2,
            'status' => 1
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 2,
            'order' => 1,
            'name' => 'New Doctor',
            'activename' => 'New Doctor',
            'icon' => 'fa-user-plus',
            'route' => 'doctors.create'
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 2,
            'order' => 2,
            'name' => 'Doctor List',
            'activename' => 'Doctor List',
            'icon' => 'fa-list',
            'route' => 'doctors.index'
        ]);

        // Default Menu with subitems Patient
        DB::table('usermenus')->insert([
            'order' => 3,
            'name' => 'Patient',
            'activename' => '',
            'icon' => 'fa-child',
            'route' => '#',
            'type' => 1,
            'role' => 2,
            'status' => 1
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 3,
            'order' => 1,
            'name' => 'New Patient',
            'activename' => 'New Patient',
            'icon' => 'fa-user-plus',
            'route' => 'patients.create'
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 3,
            'order' => 2,
            'name' => 'Patient List',
            'activename' => 'Patient List',
            'icon' => 'fa-list',
            'route' => 'patients.index'
        ]);

        // Default Menu with subitems Appointment
        DB::table('usermenus')->insert([
            'order' => 4,
            'name' => 'Appointment',
            'activename' => '',
            'icon' => 'fa-calendar-check-o',
            'route' => '#',
            'type' => 1,
            'role' => 2,
            'status' => 1
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 4,
            'order' => 1,
            'name' => 'New Appointment',
            'activename' => 'New Appointment',
            'icon' => 'fa-plus',
            'route' => 'appointments.create'
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 4,
            'order' => 2,
            'name' => 'Appointment List',
            'activename' => 'Appointment List',
            'icon' => 'fa-list',
            'route' => 'appointments.index'
        ]);

        // Default Menu with subitems Prescription
        DB::table('usermenus')->insert([
            'order' => 5,
            'name' => 'Prescription',
            'activename' => '',
            'icon' => 'fa-edit',
            'route' => '#',
            'type' => 1,
            'role' => 2,
            'status' => 1
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 5,
            'order' => 1,
            'name' => 'New Prescription',
            'activename' => 'New Prescription',
            'icon' => 'fa-plus',
            'route' => 'prescriptions.create'
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 5,
            'order' => 2,
            'name' => 'Prescription List',
            'activename' => 'Prescription List',
            'icon' => 'fa-list',
            'route' => 'prescriptions.index'
        ]);

        // Default Menu with subitems Invoice
        DB::table('usermenus')->insert([
            'order' => 6,
            'name' => 'Invoice',
            'activename' => '',
            'icon' => 'fa-dollar',
            'route' => '#',
            'type' => 1,
            'role' => 2,
            'status' => 1
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 6,
            'order' => 1,
            'name' => 'New Invoice',
            'activename' => 'New Invoice',
            'icon' => 'fa-plus',
            'route' => 'invoices.create'
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 6,
            'order' => 2,
            'name' => 'Invoice List',
            'activename' => 'Invoice List',
            'icon' => 'fa-list',
            'route' => 'invoices.index'
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 6,
            'order' => 3,
            'name' => 'New Service',
            'activename' => 'New Service',
            'icon' => 'fa-gear',
            'route' => 'services.create'
        ]);

        // Default Menu Schedule
        DB::table('usermenus')->insert([
            'order' => 7,
            'name' => 'Schedule',
            'activename' => 'Schedule List',
            'icon' => 'fa-calendar-plus-o',
            'route' => 'schedules.index',
            'type' => 0,
            'role' => 2,
            'status' => 1
        ]);

        // Default Menu with subitems User Role
        DB::table('usermenus')->insert([
            'order' => 8,
            'name' => 'User Role',
            'activename' => '',
            'icon' => 'fa-tags',
            'route' => '#',
            'type' => 1,
            'role' => 1,
            'status' => 0
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 8,
            'order' => 1,
            'name' => 'New User Role',
            'activename' => 'New User Role',
            'icon' => 'fa-tag',
            'route' => 'roles.create'
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 8,
            'order' => 2,
            'name' => 'User Role List',
            'activename' => 'User Role List',
            'icon' => 'fa-list',
            'route' => 'roles.index'
        ]);

        // Default Menu with subitems User
        DB::table('usermenus')->insert([
            'order' => 9,
            'name' => 'User',
            'activename' => '',
            'icon' => 'fa-users',
            'route' => '#',
            'type' => 1,
            'role' => 1,
            'status' => 0
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 9,
            'order' => 1,
            'name' => 'New User',
            'activename' => 'New User',
            'icon' => 'fa-user-plus',
            'route' => 'users.create'
        ]);

        DB::table('usersubmenus')->insert([
            'mainmenu' => 9,
            'order' => 2,
            'name' => 'User List',
            'activename' => 'User List',
            'icon' => 'fa-list',
            'route' => 'users.index'
        ]);

    }
}
