<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // Default User Role
        DB::table('roles')->insert([
            'name' => 'System Administrator',
            'description' => 'System Administrator'
        ]);

        DB::table('roles')->insert([
            'name' => 'Administrator',
            'description' => 'Administrator'
        ]);

        DB::table('roles')->insert([
            'name' => 'Clerk',
            'description' => 'Clerk'
        ]);

    }
}
