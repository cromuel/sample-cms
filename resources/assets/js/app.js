
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');
Vue.config.silent = true;
Vue.config.debug = false;
Vue.config.productionTip = false;

window.moment = require('moment');
window.toastr = require('toastr');
window.LoadingOverlay = require('gasparesganga-jquery-loading-overlay');

require('./toastr.js');
require('../../../public/dist/js/adminlte.js');
require('../../../public/js/intlTelInput.js');
require('../../../public/dist/plugins/datatables/jquery.dataTables.js');
require('../../../public/dist/plugins/datatables/dataTables.bootstrap4.js');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

window.eventApp = new Vue();

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});

// Select2
import select2 from "select2";
$(".select2").select2({
	placeholder: "Select an item"
});
$(".selectSearch").select2({
	placeholder: "Search by Name",
    minimumInputLength: 3 // only start searching when the user has input 3 or more characters
});

// DataTable
$(function () {
	$("#datatable").DataTable({
	  language: { emptyTable: "nothing to see here"},
	  order: [[ 0, "desc" ]]
	});
});