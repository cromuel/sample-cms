{{-- Main Footer --}}
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <i class="fa fa-rebel"></i>&nbsp;&nbsp;0.1.0
    </div>
    <strong>&copy; <?php echo date("Y");?> <a href="http://fb.com/crom.ml">cmlApps</a>. All rights reserved. </strong>
  </footer>