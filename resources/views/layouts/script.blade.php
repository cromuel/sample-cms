{{-- app.js --}}
<script src="{{ asset('js/app.js') }}" charset="utf-8"></script>
{{-- ckeditor --}}
<script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
{{-- mask --}}
<script src="{{ asset('js/jquery.mask.js') }}"></script>


@if (session('success'))
  <script>
    toastr["success"]("{{ session('success') }}");
  </script>
@elseif (session('destroy'))
  <script>
    toastr["error"]("{{ session('destroy') }}");
  </script>
@elseif (session('error'))
  <script>
    toastr["error"]("{{ session('error') }}");
  </script>
@endif


<script>
  {{-- LoadingOverlay --}}
  function LoadingOverlay() {
    $.LoadingOverlay("show", {
        image       : "{{ asset('img/heart.svg') }}"
    });
  }


  {{-- CKEDITOR --}}
  if ($("#description").attr("name") == "description") {
    CKEDITOR.replace("description", {
        toolbarGroups: [{
        "name": "basicstyles",
        "groups": ["basicstyles","clipboard","undo","links","list","indent","blocks","styles","mode"],
        }],
        extraPlugins: 'wordcount',
        wordcount: {
            showParagraphs: false,
            showWordCount: false, 
            showCharCount: true,
            maxWordCount: -1,
            maxCharCount: 250,
        }
    })
  }

  {{-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip 
  $.widget.bridge('uibutton', $.ui.button) --}}
</script>