<?php use App\Services\ImagePathService; ?>
{{-- Main Sidebar Container --}}
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    {{-- Brand Logo --}}
    <a href="{{ route('Home') }}" class="brand-link">
        <img src="{{ asset('img/logo.png') }}" alt="{{ config('app.name', 'smartCl') }}" class="brand-image img-circle elevation-3">
        <span class="brand-text font-weight-light">{{ config('app.name', 'smartCl') }}</span>
    </a>

    {{-- Sidebar --}}
    <div class="sidebar">
        {{-- Sidebar user panel (optional) --}}
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ !empty($global_user->photo) ? asset('storage/' .ImagePathService::setPath($global_user->photo)) : asset('img/profile.png') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{!! route('Profile') !!}" onclick="return LoadingOverlay();" class="d-block"> 
                    {{ $global_user->name }} 
                </a>
            </div>
        </div>

        {{-- Sidebar Menu --}}
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @foreach($global_menu->sortBy('order') as $menu)
                    @if($menu->type == 0)
                        {{-- Single Menu --}}
                        <li class="nav-item">
                            <a href="{{ route($menu->route) }}" onclick="return LoadingOverlay();" class="nav-link {{ $page_title == $menu->activename ? 'active' : '' }}">
                                <i class="nav-icon fa {{ $menu->icon }}"></i>
                                <p>
                                  {{ $menu->name }}
                                </p>
                            </a>
                        </li>
                    @else
                    {{-- Dropdown Menu --}}
                    <li class="nav-item has-treeview 
                        @foreach($menu->submenu->sortBy('order') as $submenu)
                        {{ $page_title == $submenu->activename ? 'menu-open' : 'menu-close' }}
                        @endforeach
                        ">
                        <a href="#" class="nav-link 
                        @foreach($menu->submenu->sortBy('order') as $submenu)
                        {{ $page_title == $submenu->activename ? 'active' : '' }}
                        @endforeach
                        ">
                            <i class="nav-icon fa {{ $menu->icon }}"></i>
                            <p>
                              {{ $menu->name }}
                              <i class="right fa fa-angle-left"></i>
                            </p>
                        </a> 
                            {{-- Dropdown List --}}
                            <ul class="nav nav-treeview">
                                @foreach($menu->submenu->sortBy('order') as $submenu)
                                    <li class="nav-item">
                                        <a href="{{ route($submenu->route) }}" onclick="return LoadingOverlay();" class="nav-link {{ $page_title == $submenu->activename ? 'active' : '' }}">
                                            <i class="nav-icon fa {{ $submenu->icon }}"></i>
                                            <p>{{ $submenu->name }}</p>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                    </li>
                    @endif
                @endforeach
            </ul>
        </nav>
    </div>
</aside>