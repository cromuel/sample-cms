<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content={{csrf_token()}}>

    <title>{{ config('app.name', 'smartCl') }} | {{ isset($page_title_sub) != '' ? $page_title_sub : (isset($page_title) != '' ? $page_title : 'Login') }}</title>
    {{-- favicon --}}
    <link rel="shortcut icon" href="{{ asset('img/logo.png') }}">
    {{-- app.css --}}
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    {{-- font-awesome --}}
    <link rel="stylesheet" href="{{ asset('dist/plugins/font-awesome/css/font-awesome.min.css') }}">
    {{-- Ionicons --}}
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
    {{-- DataTables --}}
    <link rel="stylesheet" href="{{ asset('dist/plugins/datatables/dataTables.bootstrap4.min.css') }}">
    {{-- Theme style --}}
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    {{-- iCheck --}}
    <link rel="stylesheet" href="{{ asset('dist/plugins/iCheck/flat/blue.css') }}">
    {{-- Date Picker --}}
    <link rel="stylesheet" href="{{ asset('dist/plugins/datepicker/datepicker3.css') }}">
    {{-- Daterange picker --}}
    <link rel="stylesheet" href="{{ asset('dist/plugins/daterangepicker/daterangepicker-bs3.css') }}">
    {{-- Google Font: Source Sans Pro --}}
    <link rel="stylesheet" href="{{ asset('css/googlefont.css') }}">
    {{-- Phone Prefix --}}
    <link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}">
    
</head>

<body class="hold-transition sidebar-mini">
    @guest @yield('content') @else
    <div class="wrapper" id="app">
        {{-- Header --}}
        @include('layouts.header')
        {{-- Sidebar --}}
        @include('layouts.sidebar') @yield('content')
        {{-- Footer --}}
        @include('layouts.footer')
    </div>
        {{-- ./wrapper --}}
        {{-- scripts --}}  
        @include('layouts.script')  
        @endguest @yield('javascript')
</body>
</html>