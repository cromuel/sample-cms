<div class="card-body">
    <!-- Name Field -->
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        <input id="name" type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" 
               name="name" value="{{ isset($role) != '' ? $role->name : old('name') }}" placeholder="Name" required>
        @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

    <!-- Description Field -->
    <div class="form-group">
        {!! Form::label('description', 'Description') !!}
    	<textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" id="description" name="description" placeholder="Enter Description">
    		{{ isset($role) != '' ? $role->description : old('description') }}
    	</textarea>
    	@if ($errors->has('description'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="card-footer">
    <!-- Submit Field -->
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="{!! route('roles.index') !!}" onclick="return LoadingOverlay();" class="btn btn-info">Cancel</a>
</div>