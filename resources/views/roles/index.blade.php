@extends('layouts.app')

@section('content')

{{-- Content Wrapper. Contains page content --}}

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">{{ $page_title }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('Home') }}" onclick="return LoadingOverlay();">Home</a></li>
            <li class="breadcrumb-item active">{{ $page_title }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary card-outline">
              <div class="card-body">
                @include('roles.table')
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  <!-- /.content -->
</div>
  
{{-- /.content-wrapper --}}

@endsection

@section('javascript')
<script>
  $(document).ready(function () {
    $('#datatableRole').DataTable({
        processing: true,
        language: {
            processing: '<span>Processing</span>',
            emptyTable: 'nothing to see here',
        },
        responsive: true,
        order: [[ 0, "desc" ]],
        serverSide: true,
        ajax: '{{ route('roles.all') }}',
        columns: [
            { data: 'id' },
            { data: 'name' },
            { data: 'description' },
            { data: 'action', searchable: false , orderable: false}
        ],
    });
  });
</script>
@stop


