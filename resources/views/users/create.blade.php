@extends('layouts.app')

@section('content')

{{-- Content Wrapper. Contains page content --}}

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">{{ $page_title }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('Home') }}" onclick="return LoadingOverlay();">Home</a></li>
            <li class="breadcrumb-item active">{{ $page_title }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary card-outline">
                {!! Form::open(['route' => 'users.store', 'files' => true, 'onsubmit' => 'return LoadingOverlay()']) !!}
                  @include('users.fields')
                {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
    </section>
  <!-- /.content -->
</div>
  
{{-- /.content-wrapper --}}

@endsection

@section('javascript')
<script>
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });

  {{-- inputPhone intlTelInput --}}
    {{-- input mask and ini --}}
    $("#inputPhone").intlTelInput({
        initialCountry: "PH",
        utilsScript: "{{ asset('js/utils.js') }}"
      });

      
    var inputPhone = document.getElementById('inputPhone');
    if (inputPhone) {
      inputPhone.addEventListener('keyup',function(){
        var phone_mask = $("#inputPhone").attr('placeholder').replace(/[0-9]/g, 0);
        $('#inputPhone').mask(phone_mask);
      });
    }

      {{-- update the hidden input on submit --}}
      $("form").submit(function() {
        $("#phone").val($("#inputPhone").intlTelInput("getNumber").replace(/\s+/g, ''));
      });
</script>
@stop