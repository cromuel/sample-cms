<div class="card-body">
    <!-- fname Field -->
    <div class="form-group">
        {!! Form::label('fname', 'First Name') !!}
        <input id="fname" type="text" class="form-control {{ $errors->has('fname') ? 'is-invalid' : '' }}" 
               name="fname" value="{{ isset($user) != '' ? $user->fname : old('fname') }}" placeholder="First Name" required>
        @if ($errors->has('fname'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('fname') }}</strong>
            </span>
        @endif
    </div>

    <!-- lname Field -->
    <div class="form-group">
        {!! Form::label('lname', 'Last Name') !!}
        <input id="lname" type="text" class="form-control {{ $errors->has('lname') ? 'is-invalid' : '' }}" 
               name="lname" value="{{ isset($user) != '' ? $user->lname : old('lname') }}" placeholder="Last Name" required>
        @if ($errors->has('lname'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('lname') }}</strong>
            </span>
        @endif
    </div>

    <!-- hidden phone Field -->
    <input id="phone" name="phone" type="hidden">

    <!-- phone Field -->
    <div class="form-group">
        {!! Form::label('phone', 'Phone') !!} <br>
        <input id="inputPhone" type="tel" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" 
               name="inputPhone" value="{{ isset($user) != '' ? $user->phone : old('phone') }}" required>
        @if ($errors->has('phone'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

    <!-- Email Field -->
    <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        <input id="email" type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" 
               name="email" value="{{ isset($user) != '' ? $user->email : old('email') }}" placeholder="Email" required>
        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <!-- role Field -->
    <div class="form-group">
        {!! Form::label('role', 'Role') !!}
        <select class="form-control {{ $errors->has('role') ? 'is-invalid' : '' }} select2" style="width: 100%;" id="role" name="role">
            <option value="0">Choose User Role</option>
            @foreach($global_roles as $global_role)
            <option {{ (isset($user) != '' ? $user->role : old('role')) == $global_role->id ? 'selected' : '' }} value="{{ $global_role->id }}">{{ $global_role->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('role'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('role') }}</strong>
            </span>
        @endif
    </div>

    <!-- image Field -->
    <div class="form-group">
        {!! Form::label('photo', 'Photo') !!}
        <div class="custom-file">
            <input type="file" class="form-control {{ $errors->has('photo') ? 'is-invalid' : '' }} custom-file-input" id="photo" name="photo" value="">
            <label class="custom-file-label" for="image">Choose file</label>
            @if ($errors->has('photo'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('photo') }}</strong>
                </span>
            @endif
        </div>
        
    </div>
</div>

<div class="card-footer">
    <!-- Submit Field -->
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="{!! route('users.index') !!}" onclick="return LoadingOverlay();" class="btn btn-info">Cancel</a>
</div>
