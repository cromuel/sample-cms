<div class="table-responsive">
    <table id="datatableUser" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Role</th>
                <th width="100px"><center>Options</center></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
